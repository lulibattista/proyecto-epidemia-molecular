using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniMenu1 : MonoBehaviour
{
    public GameObject canvasObject;
    public Rigidbody Jugador;
    // Start is called before the first frame update
    void Start()
    {
        canvasObject.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        Jugador = GetComponent<Rigidbody>();
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            canvasObject.SetActive(true);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            Jugador.constraints = RigidbodyConstraints.FreezePosition;

        }
    }

    public void SalirMiniMenu()
    {
        canvasObject.SetActive(false);
        Jugador.constraints = RigidbodyConstraints.None;
    }
}
