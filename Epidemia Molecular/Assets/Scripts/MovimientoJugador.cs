using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoJugador : MonoBehaviour
{
    Rigidbody miRB;
    public int rapidez;
    public bool canMove;
    // Start is called before the first frame update
    void Start()
    {
        miRB = GetComponent<Rigidbody>();
        rapidez = 10;
        canMove = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidez;
            float movimientoCostados = Input.GetAxis("Horizontal") * rapidez;
            movimientoAdelanteAtras *= Time.deltaTime;
            movimientoCostados *= Time.deltaTime;
            miRB.transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
        }
        else
        {
            miRB.constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;
        }
    }
}
