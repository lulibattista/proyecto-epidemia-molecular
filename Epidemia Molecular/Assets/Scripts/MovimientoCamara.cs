using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoCamara : MonoBehaviour
{
    [SerializeField] private Transform Player;

    private Vector3 direccion;
    private Rigidbody rb;
    public float velocidadMouse = 50f;

    [SerializeField] private float maxY;
    [SerializeField] private float rX;

    [SerializeField] private Transform CamRotacion;
    [SerializeField] private Transform Cam;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        direccion = Player.TransformVector(new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized);

        rX = Mathf.Lerp(rX, Input.GetAxisRaw("Mouse X") * 2, 100 * Time.deltaTime);
        maxY = Mathf.Clamp(maxY - (Input.GetAxisRaw("Mouse Y") * 2 * 100 * Time.deltaTime), -30, 30f);

        Player.Rotate(0, rX, 0, Space.World);
        Cam.rotation = Quaternion.Lerp(Cam.rotation, Quaternion.Euler(maxY * 2, Player.eulerAngles.y, 0), 100 * Time.deltaTime);

        CamRotacion.position = Vector3.Lerp(CamRotacion.position, Player.position, 10 * Time.deltaTime);
    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + direccion * 10 * Time.fixedDeltaTime);

    }
}
